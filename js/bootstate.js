var EyeBrowsGone = EyeBrowsGone || {};

EyeBrowsGone.Boot = function(){};

//setting game configuration and loading the assets for the loading screen
EyeBrowsGone.Boot.prototype = {
	preload: function() {
		
	},
	create: function() {
		//loading screen will have a white background
		this.game.stage.backgroundColor = '#fff';

		//scaling options
		this.scale.scaleMode = Phaser.ScaleManager.USER_SCALE;
		this.scale.setUserScale(2,2);
		//this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

		//THIS MAY BE NECESSARY FOR CERTAIN BROWSER CONFIGS
		//this.game.renderer.renderSession.roundPixels = true;
		//Phaser.Canvas.setImageRenderingCrisp(this.game.canvas);

		//have the game centered horizontally
		this.scale.pageAlignHorizontally = true;

		//physics system for movement
		this.game.physics.startSystem(Phaser.Physics.ARCADE);

		this.game.renderer.renderSession.roundPixels = true;

		this.state.start('Preload');
	}
};