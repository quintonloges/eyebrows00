var EyeBrowsGone = EyeBrowsGone || {};
 
EyeBrowsGone.game = new Phaser.Game(16 * 16, 16 * 15, Phaser.AUTO, '', this, false, false);
 
EyeBrowsGone.game.state.add('Boot', EyeBrowsGone.Boot);
EyeBrowsGone.game.state.add('Preload', EyeBrowsGone.Preload);
EyeBrowsGone.game.state.add('MainMenu', EyeBrowsGone.MainMenu);
 
EyeBrowsGone.game.state.start('Boot');