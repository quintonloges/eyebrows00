var EyeBrowsGone = EyeBrowsGone || {};

EyeBrowsGone.MainMenu = function() {};

EyeBrowsGone.MainMenu.prototype = {
	preload: function() {
		//EyeBrowsGone.game.state.add('World1-1', EyeBrowsGone.World1_1);

	},
	create: function() {
		this.game.add.image(0,0, 'titlescreen');

		this.tempGameData = new gameData(this.game, {
			score: 0,
			numLives: 3,
			playerName: 'CHAD',
			coinCount: 0,
			startTimer: false
		});

		this.tempGameData.addDisplayObject('CONTROLS:\nARROW KEYS TO MOVE\nZ - JUMP\nX - SPRINT', 3.5*16, 8*16);

		this.tempGameData.addDisplayObject('PRESS ANY KEY TO PLAY', 3.5*16, 11.5*16);

		this.game.input.keyboard.addCallbacks(this, this.keyDown, null, null);
	},
	keyDown: function(char) {
		this.game.state.start('PreLevel', true, false, {
			numLives: 3,
			score: 0,
			coinCount: 0,
			playerName: 'CHAD',
			worldTitle: 'WORLD 1-1',
			nextStateName: 'World1-1'
		});
	}
};