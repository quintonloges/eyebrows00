/*************************************/
/*         GAME DATA                 */
/*************************************/
gameData = function(game, gameInitValues) {
	//Make this object its own group
	Phaser.Group.call(this, game);
	this.game = game;

	//save this for later
	this.gameInitValues = gameInitValues;

	//initialize values
	this.score = gameInitValues.score;
	this.lives = gameInitValues.numLives;
	this.name = gameInitValues.playerName;
	this.time = gameInitValues.time || 400;
	this.displayGroup = this.game.add.group();
	this.numCoins = gameInitValues.coinCount;

	//create the text display objects
	this.createDisplayObjects();

	//start the world timer
	var shouldStartTimer = gameInitValues.startTimer;
	this.startTimer(shouldStartTimer);
};

gameData.prototype = Object.create(Phaser.Group.prototype);
gameData.prototype.constructor = gameData;

gameData.prototype.update = function() {
	//if the timer is running, update the display string
	if(this.gameTimer !== undefined) {
		this.setDisplayString(this.displayTimeText, 'TIME\n' + (this.time - Math.floor(this.gameTimer.seconds)));
	}	
};

gameData.prototype.createDisplayObjects = function() {
	//individually creates all the display objects
	this.displayName = this.addDisplayObject(this.name, 24, 16);
	this.displayScore = this.addDisplayObject(('' + this.score).padStart(6, '0'), 24, 24);
	this.displayWorldText = this.addDisplayObject('WORLD\n1-1', 16 * 9, 16);
	this.displayTimeText = this.addDisplayObject('TIME', 16*14.5-7*4, 16, Phaser.RetroFont.ALIGN_RIGHT);
	this.coinCounter = new worldCoinCounter(this.game, { x: 16*5.5, y:16+8} );
	this.displayCoinText = this.addDisplayObject('*' + ('' + this.numCoins).padStart(2, '0'), 16*6, 24);
};
gameData.prototype.increaseScore = function(numPoints) {
	this.score += numPoints;

	///TODO: padStart does not work in IE
	this.setDisplayString(this.displayScore, ('' + this.score).padStart(6, '0'));
};
gameData.prototype.increaseNumCoins = function() {
	this.numCoins++;

	this.setDisplayString(this.displayCoinText, '*' + ('' + this.numCoins).padStart(2, '0'));
};
gameData.prototype.addDisplayObject = function(string, x, y, alignment) {
	if(alignment === undefined) alignment = Phaser.RetroFont.ALIGN_CENTER;
	//creates the font from png file
	//'font', - loaded image asset
	//7, 7, - width/height
	//'0123...', - charmap
	//16, - chars per row
	//1, 1, - horz/vert spacing
	var curFont = this.game.add.retroFont('font', 8, 7, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ@-*!', 16, 0, 1);
	curFont.autoUpperCase = true;
	curFont.multiLine = true;
	curFont.align = alignment;
	curFont.customSpacingY = 1;
	//curFont.customSpacingX = 1;
	//create the object to hold the text
	var curText = this.game.add.image(x, y, curFont);
	curFont.text = string;
	//add it to group
	this.displayGroup.add(curText);
	//fix to camera
	curText.fixedToCamera = true;
	curText.cameraOffset.setTo(x,y);

	return { fontObj: curFont, textObj: curText, destroyMe: function() { this.textObj.destroy(); this.fontObj.destroy(); } };
};

gameData.prototype.setDisplayString = function(displayObject, string) {
	displayObject.fontObj.text = string;
};

gameData.prototype.startTimer = function(start) {
	this.gameTimer = this.game.time.create();

	this.gameTimerEvent = this.gameTimer.add(1000 * this.time, function() {}, this);
	if(start === undefined || start === true) {
		this.gameTimer.start();
	}
};
gameData.prototype.decreaseLives = function() {
	this.lives--;
};
gameData.prototype.getCurrentGameValues = function() {
	this.gameInitValues.score = this.score;
	this.gameInitValues.numLives = this.lives;
	this.gameInitValues.playerName = this.name;
	this.gameInitValues.coinCount = this.numCoins;
	return this.gameInitValues;
};