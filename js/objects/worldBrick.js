/*************************************/
/*            BRICKS                 */
/*************************************/
worldBrick = function(game, brickInitValues) {
	//create new sprite
	Phaser.Sprite.call(this,
						game,
						brickInitValues.x,
						brickInitValues.y,
						brickInitValues.sprite,
						brickInitValues.frameIndex);

	this.game = game;

	//add sprite to game
	game.add.existing(this);

	//enable physics/add body
	game.physics.arcade.enable(this);

	//set the hitbox
	this.body.setSize(12,16,2,0);

	//set immovable
	this.body.immovable = true;

	//emitter
	this.emitter = null;

	//ensure type
	this.type = 'brick';
};

worldBrick.prototype = Object.create(Phaser.Sprite.prototype);
worldBrick.prototype.constructor = worldBrick;

worldBrick.prototype.startBounceTween = function() {
	//add tween
	var bounce = this.game.add.tween(this);

	//change the y value to 4 pixels above, 100ms duration
    bounce.to({ y: this.y-4 }, 100);
    //we want it to return back to its starting position
    bounce.yoyo(true);
    //leggetit
    bounce.start();
};

worldBrick.prototype.break = function() {
	//create brick piece emitter
	this.emitter = this.game.add.emitter(this.body.x, this.body.y, 4);
	//set particle sprites to brick piece
	this.emitter.makeParticles('tiles_8x8', 3);
	this.emitter.gravity = 600;

	//TOP LEFT
	this.emitter.emitX = this.body.x;
	this.emitter.emitY = this.body.y;

	this.emitter.setXSpeed(-50,-50);
	this.emitter.setYSpeed(-200, -200);

	var particle1 = this.emitter.emitParticle();

	//TOP RIGHT
	this.emitter.emitX = this.body.x+8;
	this.emitter.emitY = this.body.y;

	this.emitter.setXSpeed(50, 50);
	this.emitter.setYSpeed(-200, -200);

	var particle2 = this.emitter.emitParticle();

	//BOTTOM RIGHT
	this.emitter.emitX = this.body.x+8;
	this.emitter.emitY = this.body.y+8;

	this.emitter.setXSpeed(50, 50);
	this.emitter.setYSpeed(-100,-100);

	var particle3 = this.emitter.emitParticle();

	//BOTTOM LEFT
	this.emitter.emitX = this.body.x;
	this.emitter.emitY = this.body.y+8;

	this.emitter.setXSpeed(-50, -50);
	this.emitter.setYSpeed(-100, -100);

	var particle4 = this.emitter.emitParticle();

	//and 3 seconds later we'll destroy the emitter
    this.game.time.events.add(3000, this.destroyEmitter, this);

	this.kill();
};

worldBrick.prototype.destroyEmitter = function() {
	this.emitter.destroy();
};