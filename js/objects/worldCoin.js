/*************************************/
/*              COIN                 */
/*************************************/
worldCoin = function(game, coinInitValues) {
	//create new sprite
	var frameIndex = 57;
	Phaser.Sprite.call(this,
						game,
						coinInitValues.x,
						coinInitValues.y,
						'tiles_00',
						frameIndex);

	//add sprite to the game
	game.add.existing(this);

	//set the anchor
	this.anchor.setTo(.5,.5);

	this.x += this.width/2;

	//animation
	this.animations.add('coin_spin',
						[
							frameIndex,
							frameIndex + 1,
							frameIndex + 2,
							frameIndex + 1
						],
						20,
						true);
	this.animations.play('coin_spin');

	//it does a tween then dies
	//add tween
	var bounce = this.game.add.tween(this);

	//change the y value to 3 tiles above, 250ms duration
    bounce.to({ y: this.y-16*3 }, 250);
    //we want it to return back to its starting position
    bounce.yoyo(true);

    //make the coin spin
    this.game.add.tween(this.scale).to({x:-1}, 250, Phaser.Easing.Linear.None).loop(true).start();

    bounce.onComplete.add(this.kill, this);
    //leggetit
    bounce.start();

    //set type
    this.type = 'coin';
};

worldCoin.prototype = Object.create(Phaser.Sprite.prototype);
worldCoin.prototype.constructor = worldCoin;