/*************************************/
/*          COIN COUNTER             */
/*************************************/
worldCoinCounter = function(game, coinCounterInitValues) {
	//create new sprite
	var frameIndex = 0;
	Phaser.Sprite.call(this,
						game,
						coinCounterInitValues.x,
						coinCounterInitValues.y,
						'tiles_8x8',
						frameIndex);

	//add sprite to the game
	game.add.existing(this);

	this.frameIndex = frameIndex;

	//animation
	this.animations.add('coin_shine',
						[
							this.frameIndex,
							this.frameIndex,
							this.frameIndex,
							this.frameIndex + 1,
							this.frameIndex + 2,
							this.frameIndex + 1
						],
						6,
						true);
	var startAnimation = coinCounterInitValues.startAnimation || true;
	if(startAnimation == true) {
		this.animations.play('coin_shine');
	}

	//fix to camera
	this.fixedToCamera = true;
	this.cameraOffset.setTo(coinCounterInitValues.x, coinCounterInitValues.y);

	//set type
	this.type = 'coinCounter';
};

worldCoinCounter.prototype = Object.create(Phaser.Sprite.prototype);
worldCoinCounter.prototype.constructor = worldCoinCounter;