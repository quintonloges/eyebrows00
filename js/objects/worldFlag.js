/*************************************/
/*                FLAG               */
/*************************************/
worldFlag = function(game, flagInitValues) {
	//create new sprite
	Phaser.Sprite.call(this,
						game,
						flagInitValues.x,
						flagInitValues.y,
						flagInitValues.sprite,
						flagInitValues.frameIndex);

	//add sprite to the game
	game.add.existing(this);

    //set type
    this.type = 'flag';
};

worldFlag.prototype = Object.create(Phaser.Sprite.prototype);
worldFlag.prototype.constructor = worldFlag;