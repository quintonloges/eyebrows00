/*************************************/
/*            FLAGPOLE               */
/*************************************/
worldFlagpole = function(game, flagpoleInitValues) {
	//create new sprite
	var frameIndex = 57;
	Phaser.Sprite.call(this,
						game,
						flagpoleInitValues.x,
						flagpoleInitValues.y,
						flagpoleInitValues.sprite,
						flagpoleInitValues.frameIndex);

	//add sprite to the game
	game.add.existing(this);

	//enable physics/add body
	game.physics.arcade.enable(this);

	//set the hitbox
	this.body.setSize(2,16,7,0);

	this.body.immovable = true;

    //set type
    this.type = 'flagpole';
};

worldFlagpole.prototype = Object.create(Phaser.Sprite.prototype);
worldFlagpole.prototype.constructor = worldFlagpole;