/*************************************/
/*             FLOWER                */
/*************************************/
worldFlower = function(game, flowerInitValues) {
	//create new sprite
	var frameIndex = 36*2;
	Phaser.Sprite.call(this,
						game,
						flowerInitValues.x,
						flowerInitValues.y,
						'tiles_01',
						frameIndex);

	//add sprite to the game
	game.add.existing(this);

	//enable physics/add body
	game.physics.arcade.enable(this);

	//slowly rise up
	this.game.add.tween(this).to({y: this.y-9}, 800, Phaser.Easing.Linear.None).start();

	//set type
	this.type = 'flower';
};

worldFlower.prototype = Object.create(Phaser.Sprite.prototype);
worldFlower.prototype.constructor = worldFlower;