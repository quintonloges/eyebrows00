/*************************************/
/*           FLOWER SHOT             */
/*************************************/
worldFlowerShot = function(game, flowerShotInitValues) {
	//create new sprite
	Phaser.Sprite.call(this,
						game,
						flowerShotInitValues.x,
						flowerShotInitValues.y,
						'tiles_8x8',
						4);
	//add sprite to game
	game.add.existing(this);

	//enable physics/add body
	game.physics.arcade.enable(this);

	this.body.gravity.y = 500;
	this.body.bounce.set(1);

	//set the anchor
	this.anchor.setTo(.5,.5);

	this.xVelocity = 100;
	if(flowerShotInitValues.direction == 'left') {
		this.xVelocity *= -1;
	}

	//set the direction
	this.body.velocity.x = this.xVelocity;

	//out of bounds
	this.autoCull = true;
	this.checkWorldBounds = true;

	//rotate
	this.body.allowRotation = true;

	//ensure type
	this.type = 'flowerShot';
};

worldFlowerShot.prototype = Object.create(Phaser.Sprite.prototype);
worldFlowerShot.prototype.constructor = worldFlowerShot;

worldFlowerShot.prototype.update = function() {
	if(!this.inWorld) {
		this.kill();
	}
	if(this.body.velocity.x > 0) {
		this.angle += 25;
	} else {
		this.angle -= 25;
	}
};

worldFlowerShot.prototype.die = function() {
	this.body.enable = false;
	this.frame = this.frame + 1;

	var timer = this.game.time.create(false);
    timer.add(100, this.kill, this);
    timer.start();
};