/*************************************/
/*             GOOMBA                */
/*************************************/
worldGoomba = function(game, goombaInitValues) {
	//create new sprite
	Phaser.Sprite.call(this,
						game,
						goombaInitValues.x,
						goombaInitValues.y,
						goombaInitValues.sprite,
						goombaInitValues.frameIndex);
	//add sprite to game
	game.add.existing(this);

	this.frameIndex = goombaInitValues.frameIndex;

	//animation
	this.animations.add('goomba_walk',
						[
							goombaInitValues.frameIndex,
							goombaInitValues.frameIndex + 1
						],
						10,
						true);
	this.animations.play('goomba_walk');

	//enable physics/add body
	game.physics.arcade.enable(this);

	this.body.gravity.y = 300;

	//set the anchor
	this.anchor.setTo(.5,.5);

	//set the hitbox
	this.body.setSize(16,16,0,16);

	this.xVelocity = 30;
	this.isMoving = false;

	//is dead
	this.dead = false;

	//ensure type
	this.type = 'goomba';
};

worldGoomba.prototype = Object.create(Phaser.Sprite.prototype);
worldGoomba.prototype.constructor = worldGoomba;

worldGoomba.prototype.update = function() {
	if(Math.abs(this.game.camera.x - this.x) < 16*20 && this.isMoving == false) {
		this.startMoving();
	}
};

worldGoomba.prototype.startMoving = function() {
	if(this.isMoving == false) {
		//set the direction
		this.body.velocity.x = -this.xVelocity;
		this.isMoving = true;
	}
	if(this.y > 16*16) {
		this.kill();
	}
};

worldGoomba.prototype.die = function() {
	this.body.velocity.x = 0;
	//change sprite to flattened goomba
	this.animations.stop();
    this.frame = this.frameIndex + 2;
    this.dead = true;
    this.body.enable = false;

    var timer = this.game.time.create(false);
    timer.add(1000, this.kill, this);
    timer.start();


};

worldGoomba.prototype.getHit = function() {
	this.body.velocity.y = -75;
	this.body.velocity.x = 0;
	this.y += 16;
	this.animations.stop();

	this.scale.y = -1;
    this.dead = true;

    var timer = this.game.time.create(false);
    timer.add(2000, this.kill, this);
    timer.start();
};