/*************************************/
/*             koopaTroopa                */
/*************************************/
worldKoopaTroopa = function(game, koopaTroopaInitValues) {
	//create new sprite
	Phaser.Sprite.call(this,
						game,
						koopaTroopaInitValues.x,
						koopaTroopaInitValues.y,
						koopaTroopaInitValues.sprite,
						koopaTroopaInitValues.frameIndex);
	//add sprite to game
	game.add.existing(this);

	this.frameIndex = koopaTroopaInitValues.frameIndex;

	//animation
	this.animations.add('koopaTroopa_walk',
						[
							koopaTroopaInitValues.frameIndex,
							koopaTroopaInitValues.frameIndex + 1
						],
						10,
						true);
	this.animations.play('koopaTroopa_walk');

	//enable physics/add body
	game.physics.arcade.enable(this);

	this.body.gravity.y = 300;

	//set the anchor
	this.anchor.setTo(.5,.5);

	//set the hitbox
	this.body.setSize(16,16,0,16);

	this.xVelocity = 30;
	this.isMoving = false;

	this.fastVelocity = 200;
	this.sliding = false;

	//is dead
	this.dead = false;

	//ensure type
	this.type = 'koopaTroopa';
};

worldKoopaTroopa.prototype = Object.create(Phaser.Sprite.prototype);
worldKoopaTroopa.prototype.constructor = worldKoopaTroopa;

worldKoopaTroopa.prototype.update = function() {
	if(Math.abs(this.game.camera.x - this.x) < 16*20 && this.isMoving == false) {
		this.startMoving();
	}
};

worldKoopaTroopa.prototype.startMoving = function() {
	if(this.isMoving == false) {
		//set the direction
		this.body.velocity.x = -this.xVelocity;
		this.isMoving = true;
	}
	if(this.y > 16*16) {
		this.kill();
	}
};

worldKoopaTroopa.prototype.die = function() {
	this.body.velocity.x = 0;
	//change sprite to koopaTroopa shell
	this.animations.stop();
    this.frame = this.frameIndex + 4;
    this.dead = true;


    this.xVelocity = this.fastVelocity;
/*
    var timer = this.game.time.create(false);
    timer.add(1000, this.kill, this);
    timer.start();
*/

};

worldKoopaTroopa.prototype.getHit = function() {
	this.body.velocity.y = -75;
	this.body.velocity.x = 0;
	this.y += 16;
	this.animations.stop();

	this.scale.y = -1;
    this.dead = true;

    var timer = this.game.time.create(false);
    timer.add(2000, this.kill, this);
    timer.start();
};