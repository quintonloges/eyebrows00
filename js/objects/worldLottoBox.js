/*************************************/
/*            LOTTO BOX              */
/*************************************/
worldLottoBox = function(game, lottoInitValues) {
	//create new sprite
	Phaser.Sprite.call(this,
						game,
						lottoInitValues.x,
						lottoInitValues.y,
						lottoInitValues.sprite,
						lottoInitValues.frameIndex);

	//add sprite to game
	game.add.existing(this);

	//animation
	this.animations.add('box_shine',
						[
							lottoInitValues.frameIndex,
							lottoInitValues.frameIndex + 1,
							lottoInitValues.frameIndex + 2,
							lottoInitValues.frameIndex + 1
						],
						5,
						true);

	this.frameIndex = lottoInitValues.frameIndex;

	this.animations.play('box_shine');

	//enable physics/add body
	game.physics.arcade.enable(this);

	//set the hitbox
	//this.body.setSize(12,16,2,0);

	//For dependency injection
	switch(lottoInitValues.payload) {
		case 'coin':
			this.rollTheDice = this.coin;
		break;
		case 'powerup':
			this.rollTheDice = this.powerup;
		break;
		default:
			this.rollTheDice = this.coin;
			//this.rollTheDice = this.powerup;
		break;
	}

	//set immovable
	this.body.immovable = true;

	//looted
	this.looted = false;

	//set type
	this.type = 'lottobox';
};

worldLottoBox.prototype = Object.create(Phaser.Sprite.prototype);
worldLottoBox.prototype.constructor = worldLottoBox;

worldLottoBox.prototype.playerCollide = function() {
	//if the box is empty return
	if(this.looted == true) {
		return;
	}

	//add tween
	var bounce = this.game.add.tween(this);

	//change the y value to 4 pixels above, 100ms duration
    bounce.to({ y: this.y-4 }, 100);
    //we want it to return back to its starting position
    bounce.yoyo(true);
    //leggetit
    bounce.start();

    this.looted = true;

    //change sprite to looted box
    this.animations.stop();
    this.frame = this.frameIndex + 3;
};

worldLottoBox.prototype.coin = function() {
	var newCoin = new worldCoin(this.game, {x: this.x, y: this.y-12});
	return newCoin;
};

worldLottoBox.prototype.powerup = function(marioType) {
	if(marioType == undefined) {
		marioType = 0;
	}
	var newItem;
	if(marioType == 0) {
		newItem = new worldMushroom(this.game, {x: this.x, y:this.y-8});	
	} else {
		newItem = new worldFlower(this.game, {x: this.x, y:this.y-8});
	}
	return newItem;
};