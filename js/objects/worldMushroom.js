/*************************************/
/*            MUSHROOM               */
/*************************************/
worldMushroom = function(game, mushroomInitValues) {
	//create new sprite
	var frameIndex = 0;
	Phaser.Sprite.call(this,
						game,
						mushroomInitValues.x,
						mushroomInitValues.y,
						'tiles_01',
						frameIndex);

	//add sprite to the game
	game.add.existing(this);

	//slowly rise up
	this.game.add.tween(this).to({y: this.y-9}, 800, Phaser.Easing.Linear.None).start().onComplete.add(this.startMoving, this);

	this.isMoving = false;

	this.xVelocity = 50;

	//set type
	this.type = 'mushroom';
};

worldMushroom.prototype = Object.create(Phaser.Sprite.prototype);
worldMushroom.prototype.constructor = worldMushroom;

worldMushroom.prototype.startMoving = function() {
	this.isMoving = true;

	this.game.physics.arcade.enable(this);

	//initial velocity
	this.body.velocity.y = 0;
	this.body.velocity.x = this.xVelocity;

	// Add gravity to make it fall
	this.body.gravity.y = 300;
};

worldMushroom.prototype.reverseDirections = function() {
	this.body.velocity.x = -this.body.velocity.x;
	this.body.velocity.y = -50;
};