/*************************************/
/*            PLAYER                 */
/*************************************/

worldPlayer = function(game, playerInitValues) {
	//create new sprite
	Phaser.Sprite.call(this,
						game,
						playerInitValues.x,
						playerInitValues.y,
						playerInitValues.sprite);

	game.add.existing(this);

	this.anchor.setTo(.5,.5);

	this.fireMarioFrame = 21;

	//player sprite sheets
	this.animations.add('walk_right', [1, 2, 3, 2], 5, true);

	this.loadTexture('ss_mario_large');
	this.animations.add('walk_right_fire', [this.fireMarioFrame + 1,
											this.fireMarioFrame + 2,
											this.fireMarioFrame + 3,
											this.fireMarioFrame + 2], 5, true);
	this.loadTexture('ss_mario_small');

	//physics
	game.physics.arcade.enable(this);

	//the camera will follow the player around the world
	game.camera.follow(this, Phaser.Camera.FOLLOW_PLATFORMER);
	//make it so the camera doesn't go backwards...
	game.camera.deadzone = new Phaser.Rectangle(0,
												0,
												game.camera.width/2,
												game.camera.height);

	// Variable to store the arrow key pressed
	this.cursor = this.game.input.keyboard.createCursorKeys();

	this.game.input.keyboard.addCallbacks(this, this.keyDown, this.keyUp, null);
	this.xReleased = true;
	this.zReleased = true;

	//initial velocity
	this.body.velocity.y = 0;
	this.body.velocity.x = 0;

	// Add gravity to make it fall
	this.body.gravity.y = 600;

	//need to do this..
	this.canJump = false;

	//for movement
	this.MAX_HOR_VELOCITY = 90;
	this.MAX_HOR_WALK_VELOCITY = 90;
	this.MAX_HOR_RUN_VELOCITY = 125;
	this.MAX_VERT_VELOCITY = 200;
	this.INCREMENTAL_SPEED = 3;

	this.MAX_JUMP_TIME = 5;
	this.JUMP_TIME = -1;

	this.GRAVITY_REGULAR = 600;
	this.GRAVITY_LIGHT = 300;

	this.curTexture = 'ss_mario_small';
	this.canThrowFlowers = false;
	this.flowerShots = this.game.add.group();
	this.dead = false;
	this.finishedLevel = false;

	this.MARIO_TYPES = {
		MARIO_SMALL: 0,
		MARIO_LARGE: 1,
		MARIO_FIRE: 2
	};

	this.marioType = this.MARIO_TYPES.MARIO_SMALL;

	//growing animation
	this.growingAnimation = -1;
	this.growingAnimationLength = 40;
	//invincibility
	this.invincibilityCounter = -1;
	this.invincibilityLength = 100;

	//UNCOMMENT THIS FOR FIRE MARIO
	/*
	this.marioType = this.MARIO_TYPES.MARIO_FIRE;
	this.canThrowFlowers = true;
	this.curTexture = 'ss_mario_large';
	this.loadTexture(this.curTexture);
	this.anchor.setTo(.5,.5);
	this.body.setSize(16,32);
	*/

	//set the type
	this.type = 'player';
};

worldPlayer.prototype = Object.create(Phaser.Sprite.prototype);
worldPlayer.prototype.constructor = worldPlayer;

worldPlayer.prototype.keyDown = function(char) {
	if(this.xReleased == true && char.key == 'x') {
		this.xReleased = false;

		//throw the flower if he's fire mario
		if(this.marioType == this.MARIO_TYPES.MARIO_FIRE) {

			//check if there's too many on the screen
			if(this.flowerShots.countLiving() == 2) {
				return;
			}

			//change the sprite
			this.loadTexture(this.curTexture, this.fireMarioFrame + 16);

			//calculate flower x pos and direction
			var posX = this.body.x;
			var dir = 'left';
			if(this.scale.x == 1) {
				posX += 16;
				dir = 'right';
			}
			//create the object
			var flowerShot = new worldFlowerShot(this.game, {
				x: posX,
				y: this.body.y + 8,
				direction: dir
			});
			//add it to group
			this.flowerShots.add(flowerShot);
		}
	}
	
	if(char.key == 'x') {
		this.MAX_HOR_VELOCITY = this.MAX_HOR_RUN_VELOCITY;
		if(this.cursor.left.isDown) {
			this.body.velocity.x -= this.INCREMENTAL_SPEED/2;
		} else if(this.cursor.right.isDown) {
			this.body.velocity.x += this.INCREMENTAL_SPEED/2;
		}
	}
	if(this.zReleased == true && char.key == 'z') {
		if(this.body.touching.down || this.body.blocked.down) {
			this.body.velocity.y -= this.MAX_VERT_VELOCITY;
			this.JUMP_TIME = 0;
		}
		this.zReleased = false;
	}
	if(char.key == 'z' && this.JUMP_TIME >= 0) {
		this.JUMP_TIME++;
		this.body.gravity.y = this.GRAVITY_LIGHT;
		if(this.JUMP_TIME >= this.MAX_JUMP_TIME) {
			this.JUMP_TIME = -1;
			this.body.gravity.y = this.GRAVITY_REGULAR;
		}
	}
};

worldPlayer.prototype.keyUp = function(char) {
	if(char.key == 'x') {
		this.xReleased = true;
		this.MAX_HOR_VELOCITY = this.MAX_HOR_WALK_VELOCITY;
	}
	if(char.key == 'z') {
		this.JUMP_TIME = -1;
		this.body.gravity.y = this.GRAVITY_REGULAR;
		this.zReleased = true;
	}
};

worldPlayer.prototype.update = function() {

	if(this.growingAnimation >= 0) {
		if(this.marioType == this.MARIO_TYPES.MARIO_SMALL) {
			this.alpha = .5;
		}
		if(this.growingAnimation % 5 == 0) {
			if(this.growingAnimation % 10 == 0) {
				if(this.marioType != this.MARIO_TYPES.MARIO_FIRE) {
					this.curTexture = 'ss_mario_small';	
				} else {
					this.curTexture = 'ss_mario_large';
					this.frame = 0;
				}
				
				this.loadTexture(this.curTexture);
			} else {
				if(this.marioType != this.MARIO_TYPES.MARIO_FIRE) {
					this.curTexture = 'ss_mario_large';	
				} else {
					this.curTexture = 'ss_mario_large';
					this.frame = this.fireMarioFrame;
				}
				this.loadTexture(this.curTexture, this.frame);
			}
		}
		this.growingAnimation++;
		if(this.growingAnimation > this.growingAnimationLength) {
			this.growingAnimation = -1;
			switch(this.marioType) {
				case this.MARIO_TYPES.MARIO_LARGE:
					this.curTexture = 'ss_mario_large';
					this.y = this.y - 8;
					this.anchor.setTo(.5,.5);
					this.body.setSize(16,32);
				break;
				case this.MARIO_TYPES.MARIO_FIRE:
					this.curTexture = 'ss_mario_large';
					this.frame = this.fireMarioFrame;
					this.canThrowFlowers = true;
				break;
				case this.MARIO_TYPES.MARIO_SMALL:
					//player shrunk
					this.curTexture = 'ss_mario_small';
					this.y = this.y + 8;
					this.anchor.setTo(.5,.5);
					this.body.setSize(16,16);
					//set invincity
					this.invincibilityCounter = 0;
				break;
			}
			this.loadTexture(this.curTexture, this.frame);
			this.body.enable = true;
		}
	}

	if(this.dead == true) {
		//change the sprite
		this.loadTexture(this.curTexture, 6, false);
	}

	//if the player can't move...
	if(this.body.enable == false) return;

	//invincibility
	if(this.invincibilityCounter >= 0) {
		this.invincibilityCounter++;
		if(this.invincibilityCounter > this.invincibilityLength) {
			this.invincibilityCounter = -1;
			this.alpha = 1;
		}
	}


	// PLAYER MOVEMENT
	//if left button down
	if (this.cursor.left.isDown) {
		//if the player is moving to the right
		if(this.body.velocity > 0) {
			//slow them down extra fast
			this.body.velocity -= this.INCREMENTAL_SPEED;
		} else {
			this.updateSprite('walk_left');
		}
		//update player
		this.body.velocity.x -= this.INCREMENTAL_SPEED;
		this.body.velocity.x = Math.max(-1*this.MAX_HOR_VELOCITY, this.body.velocity.x);
	}
	//if right button down
	else if (this.cursor.right.isDown) {
		//if the player is moving to the left
		if(this.body.velocity < 0) {
			//slow them down extra fast
			this.body.velocity += this.INCREMENTAL_SPEED;
		} else {
			this.updateSprite('walk_right');
		}
		//update player
		this.body.velocity.x += this.INCREMENTAL_SPEED;
		this.body.velocity.x = Math.min(this.MAX_HOR_VELOCITY, this.body.velocity.x);
	}
	//left or right not pressed, and velocity not 0
	else if(this.body.velocity.x != 0) {
		//if player is still sliding to right
		if(this.body.velocity.x > 0) {
			this.body.velocity.x = Math.max(0, this.body.velocity.x - this.INCREMENTAL_SPEED);
		} else{
			this.body.velocity.x = Math.min(0, this.body.velocity.x + this.INCREMENTAL_SPEED);
		}
	}
	//if the player is stopped completely, update his sprite
	if(this.body.velocity.x == 0) {
		this.updateSprite('stand_still');
	}
    
	//this.updateSprite();
/*
    //im not gonna set a listener for this so this is how i check if the player released the jump button
    if(!this.canJump && !this.cursor.up.isDown) {
    	this.canJump = true;
    }
    //make the player jump
	if(this.cursor.up.isDown && (this.body.blocked.down || this.body.touching.down) && this.canJump) {
		this.body.velocity.y = (-1*this.MAX_VERT_VELOCITY);
		this.canJump = false;
	}
*/
	//force him within camera bounds
	if(this.x-8 < this.game.camera.x) {
		this.body.velocity.x = 0;
		this.x = this.game.camera.x + 8;
	}
};

worldPlayer.prototype.bounce = function() {
	this.body.velocity.y = (-1*this.MAX_VERT_VELOCITY/1.75);
	this.canJump = false;
};

worldPlayer.prototype.ingestShrooms = function() {
	this.body.enable = false;
	//make the player grow
	this.marioType = this.MARIO_TYPES.MARIO_LARGE;
    this.growingAnimation = 0;
    this.anchor.setTo(.5,1);
	this.y = this.y+8;
};

worldPlayer.prototype.die = function() {
	//stop the player
	this.body.velocity.x = 0;
	this.body.velocity.y = 0;

	//take away player physics
	this.body.enable = false;

	this.dead = true;

	//change the sprite
	this.loadTexture(this.curTexture, 6, false);

	//pause for a sec, then slowly rise up
	var tween0 = this.game.add.tween(this).to({y: this.y}, 600, Phaser.Easing.Linear.None);
	var tween1 = this.game.add.tween(this).to({y: this.y-16*5}, 600, Phaser.Easing.Sinusoidal.InOut);
	var tween2 = this.game.add.tween(this).to({y: 16*16}, 600, Phaser.Easing.Sinusoidal.InOut);

	tween0.chain(tween1);
	tween1.chain(tween2);

	tween0.start();
	//.onComplete.add(this.startMoving, this);
};

worldPlayer.prototype.updateSprite = function(intendedSprite) {
	if(this.game.input.keyboard.downDuration(Phaser.KeyCode.X, 300) && this.frame == 16) {
		return;
	}
	switch(intendedSprite) {
		case 'walk_left':
			//set the sprite animation
			if(this.scale.x == 1) {
				this.scale.x = -1;
			}
			if(this.marioType != this.MARIO_TYPES.MARIO_FIRE) {
				this.animations.play('walk_right');	
			} else {
				this.animations.play('walk_right_fire');
			}
			
		break;
		case 'walk_right':
			//set the sprite animation
			if(this.scale.x == -1) {
				this.scale.x = 1;
			}
			if(this.marioType != this.MARIO_TYPES.MARIO_FIRE) {
				this.animations.play('walk_right');	
			} else {
				this.animations.play('walk_right_fire');
			}
		break;
		case 'stand_still':
			this.loadTexture(this.curTexture);
			if(this.marioType == this.MARIO_TYPES.MARIO_FIRE) {
				this.frame = this.fireMarioFrame;
			}
		break;
	}
};

worldPlayer.prototype.shrink = function() {
	this.body.enable = false;
	//make the player shrink
	this.marioType = this.MARIO_TYPES.MARIO_SMALL;
    this.growingAnimation = 0;
    this.anchor.setTo(.5,.5);
	this.y = this.y+8;
};

worldPlayer.prototype.isInvincible = function() {
	return (this.invincibilityCounter >= 0);
};

worldPlayer.prototype.collideFlower = function() {
	switch(this.marioType) {
		case this.MARIO_TYPES.MARIO_SMALL:
		this.ingestShrooms();
		break;
		case this.MARIO_TYPES.MARIO_LARGE:
		this.enflameMario();
		break;
	}
};

worldPlayer.prototype.enflameMario = function() {
	this.body.enable = false;
	//make the player grow
	this.marioType = this.MARIO_TYPES.MARIO_FIRE;
    this.growingAnimation = 0;
};

worldPlayer.prototype.slideDownFlagpole = function() {
	this.finishedLevel = true;

	this.body.enable = false;

	this.x += 4;

	this.animations.stop();
	if(this.marioType == this.MARIO_TYPES.MARIO_SMALL) {
		this.frame = 8;
	} else {
		if(this.marioType == this.MARIO_TYPES.MARIO_LARGE) {
			this.frame = 8;
		} else if(this.marioType == this.MARIO_TYPES.MARIO_FIRE) {
			this.frame = this.fireMarioFrame + 8;
		}
	}

	var tween0 = this.game.add.tween(this).to({y: (16 * 15) - (16*3)}, 1200, Phaser.Easing.Linear.None);
	tween0.start();
};