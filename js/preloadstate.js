var EyeBrowsGone = EyeBrowsGone || {};
 
//loading the game assets
EyeBrowsGone.Preload = function(){};
 
EyeBrowsGone.Preload.prototype = {
  preload: function() {
  	//show logo in loading screen
  	//this.splash = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'logo');
    //this.splash.anchor.setTo(0.5);
 
    //this.preloadBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY + 128, 'preloadbar');
    //this.preloadBar.anchor.setTo(0.5);
 
    //this.load.setPreloadSprite(this.preloadBar);
    this.game.stage.backgroundColor = '#000000';
    var style = { font: "65px Arial", fill: "#fffff", align: "center" };
    this.loadingText = this.add.text(this.game.world.centerX, this.game.world.centerY, 'THIS WEBSITE IS LOADING!', style);
 
  	//load game assets
  	this.game.load.tilemap('level_beta', 'assets/levels/beta01.json', null, Phaser.Tilemap.TILED_JSON);
    this.game.load.tilemap('world1-1.json', 'assets/levels/world1-1.json', null, Phaser.Tilemap.TILED_JSON);
    this.game.load.image('titlescreen', 'assets/images/title_screen.png');
    this.game.load.spritesheet('tiles_00', 'assets/images/tiles_00.png', 16, 16);
    this.game.load.spritesheet('tiles_01', 'assets/images/tiles_01.png', 16, 16);
    this.game.load.spritesheet('tiles_8x8', 'assets/images/tiles_8x8.png', 8, 8);
    this.game.load.spritesheet('enemies_00', 'assets/images/enemies_00.png', 16, 32);
    this.game.load.spritesheet('ss_mario_small', 'assets/images/spritesheet_mario_small.png', 16, 16);
    this.game.load.spritesheet('ss_mario_large', 'assets/images/spritesheet_mario_large.png', 16, 32, -1, 1, 1);

    //world states
    this.game.load.script('prelevelstate.js', 'js/worldstates/prelevelstate.js');
    this.game.load.script('gamestate.js', 'js/worldstates/gamestate.js');
    this.game.load.script('world1-1.js', 'js/worldstates/world1-1.js');

    //game objects
    this.game.load.script('worldPlayer.js', 'js/objects/worldPlayer.js');
    this.game.load.script('worldBrick.js', 'js/objects/worldBrick.js');
    this.game.load.script('worldLottoBox.js', 'js/objects/worldLottoBox.js');
    this.game.load.script('worldCoin.js', 'js/objects/worldCoin.js');
    this.game.load.script('worldMushroom.js', 'js/objects/worldMushroom.js');
    this.game.load.script('worldGoomba.js', 'js/objects/worldGoomba.js');
    this.game.load.script('gameData.js', 'js/objects/gameData.js');
    this.game.load.script('worldCoinCointer.js', 'js/objects/worldCoinCounter.js');
    this.game.load.script('worldFlowerShot.js', 'js/objects/worldFlowerShot.js');
    this.game.load.script('worldKoopaTroopa.js', 'js/objects/worldKoopaTroopa.js');
    this.game.load.script('worldFlower.js', 'js/objects/worldFlower.js');
    this.game.load.script('worldFlagpole.js', 'js/objects/worldFlagpole.js');
    this.game.load.script('worldFlag.js', 'js/objects/worldFlag.js');

    this.game.load.image('font', 'assets/images/font.png');
  },
  create: function() {
    EyeBrowsGone.game.state.add('PreLevel', EyeBrowsGone.PreLevel);
    EyeBrowsGone.game.state.add('World1-1', EyeBrowsGone.World1_1);

  	this.state.start('MainMenu');
  }
};