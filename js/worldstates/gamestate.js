/*888888888888888888888888888888888888888888888888888888888888888
  88                                                           88
  88                    GAME L00P STUFF                        88
  88                                                           88
  888888888888888888888888888888888888888888888888888888888888888*/

var EyeBrowsGone = EyeBrowsGone || {};

//title screen
EyeBrowsGone.Game = function(backgroundColor, tilemap, tilesetImage) {
	this.argBackgroundColor = backgroundColor;
	this.argTilemap = tilemap;
	this.argTilesetImage = tilesetImage;
};

EyeBrowsGone.Game.prototype = {
	init: function(gameValues) {
		this.gameValues = gameValues;
		this.gameValues.startTimer = true;
	},
	create: function() {
		//debug
		this.debug = false;

		//set world dimensions
		this.game.world.setBounds(0, 0, 16 * 100, 16 * 15);

		//background will be blue
		this.game.stage.backgroundColor = this.argBackgroundColor;

		//Add the tilemap and tileset image. The first parameter in addTilesetImage
		//is the name you gave the tilesheet when importing it into Tiled, the second
		//is the key to the asset in Phaser
		this.map = this.game.add.tilemap(this.argTilemap);
		this.map.addTilesetImage(this.argTilesetImage, this.argTilesetImage);
 
		
		var group_name, object_layer, collision_tiles;
		// create map layers
	    this.layers = {};
	    this.map.layers.forEach(function (layer) {
	        this.layers[layer.name] = this.map.createLayer(layer.name);
	        if (layer.properties.collision) { // collision layer
	            collision_tiles = [];
	            layer.data.forEach(function (data_row) { // find tiles used in the layer
	                data_row.forEach(function (tile) {
	                    // check if it's a valid tile index and isn't already in the list
	                    if (tile.index > 0 && collision_tiles.indexOf(tile.index) === -1) {
	                        collision_tiles.push(tile.index);
	                    }
	                }, this);
	            }, this);
	            this.map.setCollision(collision_tiles, true, layer.name);
	        }
	    }, this);
	    // resize the world to be the size of the current layer
	    this.layers[this.map.layer.name].resizeWorld();

	},
	update: function() {
		// COLLISIONS
		this.game.physics.arcade.collide(this.player, this.layers['GroundLayer']); 
		this.game.physics.arcade.collide(this.player, this.bricks, this.collisionPlayerBrick, null, this);
		this.game.physics.arcade.collide(this.player, this.lottoBoxes, this.collisionPlayerLottobox, null, this);
		this.game.physics.arcade.overlap(this.player, this.flowers, this.overlapPlayerFlower, null, this);
		this.game.physics.arcade.collide(this.player, this.flagpoles, this.collisionPlayerFlagpole, null, this);

		if(this.player.y > 16 * 16 && this.player.dead == false) {
			this.player.dead = true;
			this.player.body.enable = false;

			//set up the timer for new screen
			var timer = this.game.time.create(false);
    		timer.add(4000, this.retryLevel, this);
    		timer.start();
		}

		//flower shot collisions
		this.game.physics.arcade.collide(this.player.flowerShots, this.layers['GroundLayer']);
		this.game.physics.arcade.overlap(this.player.flowerShots, this.goombas, this.collisionFlowerShotsGoombas, null, this);
		this.game.physics.arcade.overlap(this.player.flowerShots, this.koopaTroopas, this.collisionFlowerShotsGoombas, null, this);
		this.game.physics.arcade.collide(this.player.flowerShots, this.bricks);
		this.game.physics.arcade.collide(this.player.flowerShots, this.lottoboxes);

		//mushroom collisions
		this.game.physics.arcade.collide(this.mushrooms, this.bricks, this.collisionBrickMushroom, null, this);
		this.game.physics.arcade.collide(this.mushrooms, this.lottoBoxes);
		this.game.physics.arcade.collide(this.mushrooms, this.layers['GroundLayer'], this.collisionGroundMushroom, null, this);
		this.game.physics.arcade.overlap(this.mushrooms, this.player, this.overlapPlayerMushroom, null, this);

		//koop troopa collisions
		this.game.physics.arcade.collide(this.koopaTroopas, this.bricks, this.collisionEnemyBlock, null, this);
		this.game.physics.arcade.collide(this.koopaTroopas, this.lottoBoxes, this.collisionEnemyBlock, null, this);
		this.game.physics.arcade.collide(this.koopaTroopas, this.layers['GroundLayer'], this.collisionGoombasGround, null, this);
		this.game.physics.arcade.collide(this.koopaTroopas, this.koopaTroopas, this.goombasOverlap, null, this);
		this.game.physics.arcade.overlap(this.koopaTroopas, this.player, this.overlapPlayerKoopaTroopa, null, this);
		this.game.physics.arcade.overlap(this.koopaTroopas, this.goombas, this.overlapKoopTroopaGoombas, null, this);

		//goomba collisions
		this.game.physics.arcade.collide(this.goombas, this.bricks, this.collisionEnemyBlock, null, this);
		this.game.physics.arcade.collide(this.goombas, this.lottoBoxes, this.collisionEnemyBlock, null, this);
		this.game.physics.arcade.collide(this.goombas, this.layers['GroundLayer'], this.collisionGoombasGround, null, this);
		this.game.physics.arcade.collide(this.goombas, this.goombas, this.goombasOverlap, null, this);
		this.game.physics.arcade.overlap(this.goombas, this.player, this.overlapPlayerGoombas, null, this);
	},
	render: function() {
		if(this.debug == true) {
			this.game.debug.body(this.player);
		}
	},
	createPlayer: function() {
		var result = this.findObjectsByType('player', this.map, 'objectsLayer');
		//there will always be a player
		this.player = this.createFromTiledObject(result[0], null, worldPlayer);
	},
	createFlag: function() {
		var result = this.findObjectsByType('flag', this.map, 'objectsLayer');
		this.flag = this.createFromTiledObject(result[0], null, worldFlag);
	},
	createItems: function(type, constructor) {
		//create items
		var itemGroup = this.game.add.group();

		result = this.findObjectsByType(type, this.map, 'objectsLayer');
		result.forEach(function(element){
			this.createFromTiledObject(element, itemGroup, constructor);
		}, this);
		return itemGroup;
	},
	//find objects in a Tiled layer that containt a property called "type" equal to a certain value
	findObjectsByType: function(type, map, layer) {
		var result = new Array();
		map.objects[layer].forEach(function(element){
			if(element.properties.type === type) {
				//Phaser uses top left, Tiled bottom left so we have to adjust the y position
				//also keep in mind that the cup images are a bit smaller than the tile which is 16x16
				//so they might not be placed in the exact pixel position as in Tiled
				element.y -= map.tileHeight;
				result.push(element);
			}
		});
		return result;
	},
	//create a sprite from an object
    createFromTiledObject: function(element, group, constructor) {
    	var item = {};
    	item.x = element.x;
    	item.y = element.y;
    	item.sprite = element.properties.sprite;
    	item.frameIndex = element.properties.frameIndex;

    	//copy all properties to the item
    	Object.keys(element.properties).forEach(function(key){
    		if(key != 'sprite' && key != 'frameIndex') {
    			item[key] = element.properties[key];
    		}
     	});

    	//Dependency injection :D
    	var createdObj = new constructor(this.game, item);

    	//copy all properties to the object
    	Object.keys(element.properties).forEach(function(key){
    		if(key != 'sprite' && key != 'frameIndex') {
    			createdObj[key] = element.properties[key];
    		}
     	});

    	//add it to the group
    	if(group !== null)group.add(createdObj);

    	return createdObj;
    },
    collisionPlayerBrick: function(player, brick) {
    	//if the player is colliding with the brick from below
    	//then make the brick bounce
    	if(brick.body.touching.down) {
    		if(player.curTexture == 'ss_mario_small') {
    			brick.startBounceTween();	
    		} else if(player.curTexture == 'ss_mario_large') {
    			this.curGameData.increaseScore(50);
    			brick.break();
    		}
    	}
    },
    collisionPlayerLottobox: function(player, lottobox) {
    	//if the player is colliding with the brick from below
    	//then make the brick bounce
    	if(lottobox.body.touching.down) {

    		//check if lottobox is looted
    		var looted = lottobox.looted;

    		lottobox.playerCollide();

    		//just return if lottobox was already looted
    		if(looted == true) return;

    		//we need the payload so it can be added to its group
    		var payload = lottobox.rollTheDice(this.player.marioType);
    		switch(payload.type) {
    			case 'mushroom':
    			this.mushrooms.add(payload);
    			break;
    			case 'flower':
    			this.flowers.add(payload);
    			break;
    			case 'coin':
    			this.createScoreEvent(100, lottobox.body.x-8, lottobox.body.y);
    			this.curGameData.increaseNumCoins();
    			break;
    		}
    	}
    },
    collisionPlayerFlagpole: function(player, flagpole) {
    	if(player.finishedLevel == true) {
    		return;
    	}
    	player.slideDownFlagpole();
    	this.createScoreEvent(flagpole.points, player.body.x-8, player.body.y);

    	var timer = this.game.time.create(false);
    	timer.add(3000, function() {
    		var gameData = this.curGameData.getCurrentGameValues();
			gameData.gameWon = true;
			this.game.state.start('PreLevel', true, false, gameData);
    	}, this);
    	timer.start();
    },
    overlapPlayerFlower: function(player, flower) {
    	this.createScoreEvent(1000, flower.body.x-8, flower.body.y);
    	player.collideFlower();
    	flower.kill();
    },
    collisionBrickMushroom: function(mushroom, brick) {
    	if(brick.body.touching.down) {
    		//this means the player is bumping from underneath
    		mushroom.reverseDirections();
    	}
    },
    collisionGroundMushroom: function(mushroom, groundLayer) {
    	if(mushroom.body.blocked.left) {
    		mushroom.body.velocity.x = mushroom.xVelocity;
    	} else if(mushroom.body.blocked.right) {
    		mushroom.body.velocity.x = -mushroom.xVelocity;
    	}
    },
    overlapPlayerMushroom: function(player, mushroom) {
    	this.createScoreEvent(1000, mushroom.body.x, mushroom.body.y);
    	player.ingestShrooms();
    	mushroom.kill();
    },
    overlapPlayerGoombas: function(player, goomba) {
    	if(goomba.dead == true) return;
    	if(goomba.body.touching.up) {
    		if(goomba.dead == false) {
    			this.createScoreEvent(100, goomba.body.x, goomba.body.y);
    			player.bounce();
    			goomba.die();
    		}
    	} else if(player.body.touching.right || player.body.touching.left){
    		//return if he's invincible
    		if(player.isInvincible() == true) {
    			return;
    		}

    		if(player.marioType == player.MARIO_TYPES.MARIO_LARGE ||
    			player.marioType == player.MARIO_TYPES.MARIO_FIRE) {
    			player.shrink();
    		} else {
    			player.die();

    			//set up the timer for new screen
				var timer = this.game.time.create(false);
    			timer.add(4000, this.retryLevel, this);
    			timer.start();
    		}
    	}
    },
    overlapPlayerKoopaTroopa: function(player, koopaTroopa) {
    	if(koopaTroopa.dead == true && koopaTroopa.sliding == false) {
    		if(player.body.velocity.y < 0) return true;
    		if(player.x > koopaTroopa.x) {
    			koopaTroopa.body.velocity.x = (-1)*koopaTroopa.xVelocity;
    		} else {
    			koopaTroopa.body.velocity.x = koopaTroopa.xVelocity;
    		}
    		koopaTroopa.sliding = true;
    		if(koopaTroopa.body.touching.up) {
    			player.bounce();
    		}
    		return true;
    	}
    	if(koopaTroopa.body.touching.up) {
    		if(koopaTroopa.dead == false) {
    			this.createScoreEvent(100, koopaTroopa.body.x, koopaTroopa.body.y);
    			player.bounce();
    			koopaTroopa.die();
    		}
    	} else if(player.body.touching.right || player.body.touching.left){
    		//return if he's invincible
    		if(player.isInvincible() == true) {
    			return false;
    		}

    		if(player.marioType == player.MARIO_TYPES.MARIO_LARGE ||
    			player.marioType == player.MARIO_TYPES.MARIO_FIRE) {
    			player.shrink();
    		} else {
    			koopaTroopa.body.enable = false;
    			player.die();

    			//set up the timer for new screen
				var timer = this.game.time.create(false);
    			timer.add(4000, this.retryLevel, this);
    			timer.start();
    		}
    	}
    	return true;
    },
    collisionGoombasGround: function(goomba, groundLayer) {
    	if(goomba.body.blocked.left) {
    		goomba.body.velocity.x =goomba.xVelocity;
    	} else if(goomba.body.blocked.right) {
    		goomba.body.velocity.x = -goomba.xVelocity;
    	}
    	return true;
    },
    overlapKoopTroopaGoombas: function(koopaTroopa, goomba) {
    	if(koopaTroopa.dead == true && koopaTroopa.body.velocity.x != 0) {
    		this.createScoreEvent(100, goomba.body.x, goomba.body.y);
    		this.goombas.remove(goomba);
    		this.deadGoombas.add(goomba);
    		goomba.getHit();
    	}
    },
    goombasOverlap: function(goomba1, goomba2) {
    	if(goomba1.body.touching.left) {
    		goomba1.body.velocity.x = goomba1.xVelocity;
    		goomba2.body.velocity.x = -goomba2.xVelocity;
    	} else if(goomba1.body.touching.right) {
    		goomba1.body.velocity.x = -goomba1.xVelocity;
    		goomba2.body.velocity.x = goomba2.xVelocity;
    	}
    	return true;
    },
    collisionFlowerShotsGoombas: function(flowerShot, goomba) {
    	this.createScoreEvent(100, goomba.body.x, goomba.body.y);
    	flowerShot.die();
    	this.goombas.remove(goomba);
    	this.deadGoombas.add(goomba);
    	goomba.getHit();
    },
    collisionEnemyBlock: function(enemy, block) {
    	if(block.body.touching.down) {
    		//this means the player is bumping from underneath
    		this.createScoreEvent(100, enemy.body.x, enemy.body.y);
    		switch(enemy.type) {
    			case 'goomba':
    				this.goombas.remove(enemy);
    			break;
    			case 'koopaTroopa':
    				this.koopaTroopas.remove(enemy);
    			break;
    		}
    		this.deadGoombas.add(enemy);
    		enemy.getHit();
    	}
    },
    createScoreEvent: function(scoreIncrease, xPos, yPos) {
    	this.curGameData.increaseScore(scoreIncrease);
    	var thisScoreObj = this.curGameData.addDisplayObject('' + scoreIncrease, xPos, yPos);
    	thisScoreObj.textObj.fixedToCamera = false;

    	var thisTween = this.game.add.tween(thisScoreObj.textObj.position).to({y:yPos-32}, 400, Phaser.Easing.Linear.None).start();
    	thisTween.onComplete.add(thisScoreObj.destroyMe, thisScoreObj);
    },
    retryLevel: function() {
    	this.curGameData.decreaseLives();
    	this.game.state.start('PreLevel', true, false, this.curGameData.getCurrentGameValues());
    },
    logObjects: function() {
    	// Loop over each object layer
		for (var ol in this.map.objects) {
			// Loop over each object in the object layer
			for (var o in this.map.objects[ol]) {
				var object = this.map.objects[ol][o];
				
				console.log(object);
			}
		}
    }
};