var EyeBrowsGone = EyeBrowsGone || {};

EyeBrowsGone.PreLevel = function() {};

EyeBrowsGone.PreLevel.prototype = {
	init: function(preLoadInitValues) {
		this.gameValues = preLoadInitValues;
		this.numLives = preLoadInitValues.numLives;
		this.score = preLoadInitValues.score;
		this.coinCount = preLoadInitValues.coinCount;
		this.playerName = preLoadInitValues.playerName;
		this.worldTitle = preLoadInitValues.worldTitle;
		this.nextStateName = preLoadInitValues.nextStateName;
		if(preLoadInitValues.gameWon == undefined) {
			this.gameWon = false;
		} else {
			this.gameWon = preLoadInitValues.gameWon;
		}
	},
	create: function() {
		//background will be black
		this.game.stage.backgroundColor = '#000000';

		this.tempGameData = new gameData(this.game, {
			score: this.score,
			numLives: this.numLives,
			playerName: this.playerName,
			coinCount: this.coinCount,
			startTimer: false
		});

		if(this.gameWon == true) {
			this.tempGameData.addDisplayObject('YOU WIN\n\nQUINTON RULES', 16 * 5, 16 * 4.5);
		} else {
			if(this.numLives > 0) {
				this.tempGameData.addDisplayObject(this.worldTitle, 16 * 6, 16 * 4.5);
				this.game.add.sprite(16 * 6.5, 16 * 6, 'ss_mario_small', 0);
				this.tempGameData.addDisplayObject('* ' + ('' + this.numLives).padStart(2, ' '), 16 * 8, 16* 6.5);
			} else {
				this.tempGameData.addDisplayObject('GAME OVER\n\nCHAD SMELLS', 16 * 5, 16 * 4.5);
			}
		}

		//set up the timer for next level
		var timer = this.game.time.create(false);
    	timer.add(3000, this.startWorld, this);
    	timer.start();
	},
	startWorld: function() {
		if(this.numLives > 0 && this.gameWon == false) {
			this.game.state.start(this.nextStateName, true, false, this.gameValues);	
		} else {
			this.game.state.start('MainMenu', true, false);
		}
		
	}
};