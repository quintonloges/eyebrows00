var EyeBrowsGone = EyeBrowsGone || {};

EyeBrowsGone.World1_1 = function() {
	EyeBrowsGone.Game.call(this,
							'#63adff',
							'world1-1.json',
							'tiles_00');
};

EyeBrowsGone.World1_1.prototype = Object.create(EyeBrowsGone.Game.prototype);
EyeBrowsGone.World1_1.prototype.constructor = EyeBrowsGone.World1_1;

EyeBrowsGone.World1_1.prototype.create = function() {
	EyeBrowsGone.Game.prototype.create.call(this);
	//console log all objects
	//this.logObjects();

	//set up mushrooms & flowers
	this.mushrooms = this.game.add.group();
	this.flowers = this.game.add.group();

	//create lotto boxes
	this.lottoBoxes = this.createItems('lottobox', worldLottoBox);

	//create bricks
	this.bricks = this.createItems('brick', worldBrick);

	//create goombas
	this.goombas = this.createItems('goomba', worldGoomba);
	this.deadGoombas = this.game.add.group();

	//create koopa troopas
	this.koopaTroopas = this.createItems('koopaTroopa', worldKoopaTroopa);

	//create flagpole
	this.flagpoles = this.createItems('flagpole', worldFlagpole);
	this.createFlag();

	//the player
	this.createPlayer();

	//create the game data object
	this.curGameData = new gameData(this.game, this.gameValues);
};